package com.bart.test.data;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.bart.fare.FareCalculatorPage;
import com.bart.page.home.HomePage;
import com.bart.test.base.BaseTest;

public class DataProTest extends BaseTest {
	HomePage home;
	FareCalculatorPage farePage;

	@Test
	public void fromTextBoxTest() {
		home = new HomePage();
		home.mouseOverOnFaresMenu();
		farePage = home.fareCalcLinkClick();
	}
	
	@DataProvider(name="data")
	public Object[][] dataProviderTest() {
		Object [][] data = {{"Concord","Glen Park (SF)","3:30 PM"},{"Concord","Coliseum","9:30 PM"}};
		return data;
	}
	@Test(dataProvider="data",dependsOnMethods="fromTextBoxTest")
	public void dataDropDownTest(String fromSta,String toSta, String time) {
		farePage.fromDropDown(fromSta);
		farePage.toDropDown(toSta);
		farePage.timeDropDown(time);
		farePage.clickGoButton();
		System.out.println("fare calculation details "+farePage.getFareCalDetails());
	}
	
}
