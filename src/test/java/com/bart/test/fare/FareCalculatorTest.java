package com.bart.test.fare;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.bart.fare.FareCalculatorPage;
import com.bart.page.home.HomePage;
import com.bart.test.base.BaseTest;

public class FareCalculatorTest extends BaseTest {
	FareCalculatorPage farePage;
	HomePage home;
	String[] StationList = { "12th St. Oakland City Center", "16th St. Mission (SF)", "19th St. Oakland",
			"24th St. Mission (SF)", "Antioch", "Ashby (Berkeley)", "Balboa Park (SF)", "Bay Fair (San Leandro)",
			"Castro Valley", "Civic Center / UN Plaza", "Coliseum", "Colma", "Concord", "Daly City",
			"Downtown Berkeley", "Dublin / Pleasanton", "El Cerrito del Norte", "El Cerrito Plaza", "Embarcadero (SF)",
			"Fremont", "Fruitvale (Oakland)", "Glen Park (SF)", "Hayward", "Lafayette", "Lake Merritt (Oakland)",
			"MacArthur (Oakland)", "Millbrae", "Montgomery St. (SF)", "North Berkeley", "North Concord / Martinez",
			"Oakland International Airport", "Orinda", "Pittsburg / Bay Point", "Pittsburg Center",
			"Pleasant Hill / Contra Costa Centre", "Powell St. (SF)", "Richmond", "Rockridge (Oakland)", "San Bruno",
			"San Francisco International Airport", "San Leandro", "South Hayward", "South San Francisco", "Union City",
			"Walnut Creek", "Warm Springs / South Fremont", "West Dublin / Pleasanton", "West Oakland" };

	@Test
	public void fromTextBoxTest() {
		home = new HomePage();
		home.mouseOverOnFaresMenu();
		farePage = home.fareCalcLinkClick();
		Assert.assertTrue(farePage.isFromTextBoxPresent(), "from textbox is not present");
	}

	@Test(dependsOnMethods = "fromTextBoxTest")
	public void toTextBoxTest() {
		Assert.assertTrue(farePage.isToTextBoxPresent(), "text box is not present");
	}

	@Test(dependsOnMethods = "toTextBoxTest")
	public void dateTextBoxTest() {
		Assert.assertTrue(farePage.isDateTextBoxPresent(), "date text box is not present");
	}

	@Test(dependsOnMethods = "dateTextBoxTest")
	public void timeBoxTest() {
		Assert.assertTrue(farePage.isTimeBoxPresent(), "time box is not present");
	}

	@Test(dependsOnMethods = "timeBoxTest")
	public void goButtonTest() {
		Assert.assertTrue(farePage.isGoPresent(), "go button is not present");
	}

	@Test(dependsOnMethods = "goButtonTest")
	public void fare_calTitleTest() {
		Assert.assertTrue(farePage.isFareCalculatorTitlePresent(), "fare calculator title is not present");
	}

	@Test(dependsOnMethods = "fare_calTitleTest")
	public void homeLinkTest() {
		Assert.assertTrue(farePage.isHomeLinKPresent(), "home link is not present");
	}

	@Test(dependsOnMethods = "homeLinkTest")
	public void radio_oneButtonTest() {
		Assert.assertTrue(farePage.isRadioOneButtonPresent(), "radio one button is not present");
	}

	@Test(dependsOnMethods = "radio_oneButtonTest")
	public void radio_twoButtonTest() {
		Assert.assertTrue(farePage.isRadioTwoButtonPresent(), "radio button two is not present");
	}

	@Test(dependsOnMethods = "radio_twoButtonTest")
	public void fromDropDownValidationTest() {
		List<WebElement> options = farePage.fromDropDownGetOptions();

		for (int i = 0; i < options.size(); i++) {
			WebElement opt = options.get(i);
			System.out.println(opt.getText() + " " + StationList[i]);

			Assert.assertTrue(opt.getText().equals(StationList[i]),
					"expected values is not matching with actual values");
		}
	}

	@Test(dependsOnMethods = "fromDropDownValidationTest")
	public void toDropDownValidationTest() {
		List<WebElement> to = farePage.toDropDownGetOptions();
		for (int i = 0; i < to.size(); i++) {
			String data = to.get(i).getText();
			boolean present =false;

			for (int j = 0; j < StationList.length; j++) {

				System.out.println(data + " is at " + j);
				if (data.equals(StationList[j])) {
					present =true;
									}

			}
			Assert.assertTrue(present, "data is not matching with the station list");

		}
	}

	@Test(dependsOnMethods = "radio_twoButtonTest")
	public void fareValidationTest() {

		farePage.fromDropDown(3);
		farePage.toDropDown("Concord");
		farePage.timeDropDown(3);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		farePage.clickGoButton();

		String FareDetails = farePage.getFareCalDetails();
		System.out.println(FareDetails);
	}
	@DataProvider(name="stations")
	public Object[][] fromDrDownTest(){
		Object[][] sta = {{"12th St. Oakland City Center"},{"16th St. Mission (SF)"},{"19th St. Oakland"},{"Colma"},{"Concord"},{"Daly City"},{"Fremont"},{"Union City"},{"Orinda"},{"Millbrae"},{"Hayward"}};
		return sta;
	}

	
	@Test(dataProvider="stations")
	public void fDropDownTest(String sta) {
		List<WebElement>from=farePage.fromDropDownGetOptions();
		boolean prsent =false;
		for(int i=0;i<from.size();i++) {
			String fr=from.get(i).getText();
			if(fr.equals(sta)){
			prsent =true;
			}
		}
		Assert.assertTrue(prsent, "sta data is not matching with Stations");
	}

}
