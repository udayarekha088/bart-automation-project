package com.bart.test.home;



import org.testng.Assert;
import org.testng.annotations.Test;

import com.bart.fare.FareCalculatorPage;
import com.bart.page.home.HomePage;
import com.bart.test.base.BaseTest;

public class HomeTest extends BaseTest {
	HomePage home;
	FareCalculatorPage farePage;
	
	
	@Test
	public void homePageTitleTest() {
		home = new HomePage();
		String actualTitle = home.getHomePageTitle();
		String expectedTitle = "bart.gov | Bay Area Rapid Transit";
		Assert.assertEquals(actualTitle, expectedTitle, "Home Page Title is not matched with the expected");
		// Assert.assertTrue(actual.equals(expected), "Home Page Title is not matched
		// with the expected");
	}

	@Test(dependsOnMethods="homePageTitleTest")
	public void scheduleMenuTest() {

		home.isSchMenuPresent();
		Assert.assertTrue(home.isSchMenuPresent(), "sch menu is not present");
	}

	@Test(dependsOnMethods = "scheduleMenuTest")
	public void stationMenuTest() {
		boolean issame = home.isStMenuPresent();
		Assert.assertTrue(issame, "station menu is not present");
	}

	@Test(dependsOnMethods = "stationMenuTest")
	public void bartMenuTest() {

		Assert.assertTrue(home.isBartMenuPresent(), "bart menu is not Present");

	}

	@Test(dependsOnMethods = "bartMenuTest")
	public void faresMenuTest() {
		home.isFaresMenuPresent();
		Assert.assertTrue(home.isFaresMenuPresent(), "fares menu is not present");
	}

	@Test(dependsOnMethods = "faresMenuTest")
	public void newsMenuTest() {
		home.isNewsMenuPresent();
		Assert.assertTrue(home.isNewsMenuPresent(), "news menu is not present");

	}

	@Test(dependsOnMethods = "newsMenuTest")
	public void aboutMenuTest() {
		Assert.assertTrue(home.isAboutMenuPresent(), "about menu is not present");
	}

	@Test(dependsOnMethods = "aboutMenuTest")
	public void searchButtonTest() {

		Assert.assertTrue(home.isSearchButtonPresent(), "search menu is not present");
	}

	@Test(dependsOnMethods = "searchButtonTest")
	public void trip_plrLinkTest() {
		Assert.assertTrue(home.isTrip_plrLinkPresent(), "trip planner link is not present");
	}
	@Test(dependsOnMethods = "trip_plrLinkTest")
	public void schd_mapLinkTest() {
		Assert.assertTrue(home.isSched_MapLinkPresent(), "schdeule map link is not present");
	}

	@Test(dependsOnMethods = "schd_mapLinkTest")
	public void faresMenuMouseOverTest() {
		home.mouseOverOnFaresMenu();
		Assert.assertTrue(home.isFareCalcLinkPresent(), "fares calc submenu is not present");

	}

	@Test(dependsOnMethods = "faresMenuMouseOverTest")
	public void fareCalcLinkClickTest() {
		//home.fareCalcLinkClick();
		farePage = home.fareCalcLinkClick();
		String actual=farePage.getFareCalPageTitle();
		String expected ="Fare Calculator | bart.gov";
		Assert.assertEquals(actual, expected,"farecalculator page title is not matching with expected");
	}
}
