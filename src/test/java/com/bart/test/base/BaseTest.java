package com.bart.test.base;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.bart.page.base.BasePage;
import com.bart.utils.ConfigurationProperty;

public class BaseTest {
	public WebDriver driver;

	@BeforeSuite
	public void inIt() {
		String browser=ConfigurationProperty.configTest("browser");
		if(browser.equals("firefox")) {
			System.setProperty("webdriver.gecko.driver", "C:\\Users\\kdoppalapudi\\Desktop\\geckodriver.exe");
			driver=new FirefoxDriver();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			System.out.println("browser is "+browser);
		}
		else if(browser.equals("chrome")) {
			System.setProperty("webdriver.gecko.driver", "C:\\Users\\kdoppalapudi\\Desktop\\chromedriver.exe");
			driver =new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
			
		}
		BasePage.driver =driver;
		String url=ConfigurationProperty.configTest("url");
		driver.get(url);
		System.out.println("url is "+ url);
		
	}
	@AfterSuite
	public void tearDown() {
		driver.close();
	}
}
