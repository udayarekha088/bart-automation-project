package com.bart.fare;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.bart.page.base.BasePage;

public class FareCalculatorPage extends BasePage {
	@FindBy(id = "edit-orig-list")
	WebElement fromTBox;

	@FindBy(id = "edit-dest-list")
	WebElement toTBox;

	@FindBy(id = "date-popup-datepicker-popup-0")
	WebElement dateTbox;

	@FindBy(id = "edit-time")
	WebElement timeTBox;

	@FindBy(id = "edit-submit")
	WebElement go;

	@FindBy(id = "page-title")
	WebElement fareCalc;

	@FindBy(xpath = "//a[@class='easy-breadcrumb_segment easy-breadcrumb_segment-front']")
	WebElement home;

	@FindBy(xpath = "//label[contains(text(),'One-way')]")
	WebElement radio_one;

	@FindBy(xpath = "//label[contains(text(),'Round-trip')]")
	WebElement radio_two;

	@FindBy(xpath = "//strong[contains(text(),'Clipper')]")
	WebElement fareCalDet;

	public String getFareCalPageTitle() {
		return driver.getTitle();
	}

	public boolean isFromTextBoxPresent() {
		return isElementPresent(fromTBox);
	}

	public boolean isToTextBoxPresent() {
		return isElementPresent(toTBox);
	}

	public boolean isDateTextBoxPresent() {
		return isElementPresent(dateTbox);
	}

	public boolean isTimeBoxPresent() {
		return isElementPresent(timeTBox);
	}

	public boolean isGoPresent() {
		return isElementPresent(go);
	}

	public boolean isFareCalculatorTitlePresent() {
		return isElementPresent(fareCalc);
	}

	public boolean isHomeLinKPresent() {
		return isElementPresent(home);
	}

	public boolean isRadioOneButtonPresent() {
		return isElementPresent(radio_one);
	}

	public boolean isRadioTwoButtonPresent() {
		return isElementPresent(radio_two);
	}

	public void fromDropDown(int index) {
		dropDownSelectByIndex(fromTBox, index);
	}
	public void fromDropDown(String text) {
		dropDownSelectByText(fromTBox, text);
	}
	public void timeDropDown(String text) {
		dropDownSelectByText(timeTBox, text);
	}
	public List<WebElement> fromDropDownGetOptions() {
		return dropDownGetOptions(fromTBox);
	}
	public List<WebElement>toDropDownGetOptions(){
		return dropDownGetOptions(toTBox);
	}

	public void toDropDown(String text) {
		dropDownSelectByText(toTBox, text);
	}

	public void timeDropDown(int index) {
		dropDownSelectByIndex(timeTBox, index);
	}

	public void clickGoButton() {
		clickElement(go);
	}

	public boolean isfareCalcDetPresent() {
		return isElementPresent(fareCalDet);
	}

	public String getFareCalDetails() {
		return getElementText(fareCalDet);
		
	}
}
