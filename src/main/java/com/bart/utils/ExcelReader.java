package com.bart.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelReader {
	
	private static XSSFWorkbook workbook;
	private static XSSFSheet ExcelSheet;
	private static XSSFCell Cell;
	
	
	public static Object[][] getTableArray() {
		 
		Object[][] tabArray = null;
		 
		 try {
			
			FileInputStream file = new FileInputStream("src/test/resources/testdata/TestDataSets.xlsx");
			workbook = new XSSFWorkbook(file);
			ExcelSheet = workbook.getSheet("FareCalcTest");
			int TotalRows = ExcelSheet.getLastRowNum();
			int TotalColumns = ExcelSheet.getRow(1).getLastCellNum();
			
			tabArray = new String[TotalRows][TotalColumns];
			
			System.out.println("number of rows are : "+TotalRows + " and Columns are :"+TotalColumns);
			
			int ci,cj;
			
			ci=0;
			
			for(int i=1;i<= TotalRows;i++,ci++) {
				cj=0;
				
				for (int j=0;j<TotalColumns;j++,cj++) {
					try {
						
						tabArray[ci][cj] = 	getCellData(i,j);
					System.out.println(tabArray[ci][cj]);
					}
					catch(Exception e){
						break;
					}
				}
				
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		 
		 return tabArray;
	}
	
	public static Object getCellData(int RowNum, int ColNum) throws Exception {

		try {

			Cell = ExcelSheet.getRow(RowNum).getCell(ColNum);

			int dataType = Cell.getCellType();

			if (dataType == 3) {

				return "";

			} else if (dataType == 0) {
				Object CellData = Cell.getRawValue();
				return CellData;

			} else {

				Object CellData = Cell.getStringCellValue();
				return CellData;
			}

		} catch (Exception e) {

			System.out.println(e.getMessage());

			throw (e);

		}
	}

	
	/*public static void main(String[] args0) {
		System.out.println(getTableArray());
	}*/
}
