package com.bart.page.home;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.bart.fare.FareCalculatorPage;
import com.bart.page.base.BasePage;

public class HomePage extends BasePage {

	@FindBy(xpath = "//*[@id=\"block-system-main-menu\"]/div/ul/li[1]/a")
	WebElement schMenu;

	@FindBy(xpath = "//*[@id=\"block-system-main-menu\"]/div/ul/li[2]/a")
	WebElement stMenu;

	@FindBy(xpath = "//*[@id=\"block-system-main-menu\"]/div/ul/li[3]/a")
	WebElement bartMenu;

	@FindBy(xpath = "//*[@id=\"block-system-main-menu\"]/div/ul/li[4]/a")
	WebElement faresMenu;

	@FindBy(xpath = "//*[@id=\"block-system-main-menu\"]/div/ul/li[5]/a")
	WebElement newsMenu;

	@FindBy(xpath = "//*[@id=\"block-system-main-menu\"]/div/ul/li[6]/a")
	WebElement about;

	@FindBy(xpath = "//div[@class='search-icon']")
	WebElement searchButton;

	@FindBy(xpath = "//a[@class='rw planner']")
	WebElement trip_Plr;

	@FindBy(xpath = "//a[@class='rw departures']")
	WebElement rt_deptrs;

	@FindBy(xpath = "//a[contains(@class,'rw sche')]")
	WebElement sched_map;

	@FindBy(xpath = "//a[contains(text(),'Fare Calculator')]")
	List<WebElement> fareCalc_links;

	public String getHomePageTitle() {
		return driver.getTitle();
	}

	public boolean isSchMenuPresent() {
		boolean isSame = isElementPresent(schMenu);
		return isSame;
	}

	public boolean isStMenuPresent() {
		return isElementPresent(stMenu);
	}

	public boolean isBartMenuPresent() {
		return isElementPresent(bartMenu);
	}

	public boolean isFaresMenuPresent() {
		return isElementPresent(faresMenu);
	}

	public boolean isNewsMenuPresent() {
		return isElementPresent(newsMenu);
	}

	public boolean isAboutMenuPresent() {
		return isElementPresent(about);
	}
	
	public boolean isSearchButtonPresent() {
		return isElementPresent(searchButton);
	}
	public boolean isTrip_plrLinkPresent(){
		return isElementPresent(trip_Plr);
	}
	public boolean isrt_deptrsLinkPresentTest() {
		return isElementPresent(rt_deptrs);
	}
	
	public boolean isSched_MapLinkPresent() {
		return isElementPresent(sched_map);
	}

	public void mouseOverOnFaresMenu() {
		
		mouseOver(faresMenu);
	}

	public boolean isFareCalcLinkPresent() {
		return isElementPresent(fareCalc_links.get(0));
	}

	public FareCalculatorPage  fareCalcLinkClick() {
		
		WebDriverWait wait =new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOf(fareCalc_links.get(0)));
		clickElement(fareCalc_links.get(0));
		return new FareCalculatorPage();
	}
}
