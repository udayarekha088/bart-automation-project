package com.bart.page.base;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class BasePage {
	public static WebDriver driver;

	public BasePage() {
		// TODO Auto-generated constructor stub
		PageFactory.initElements(driver, this);
	}
	
	protected boolean isElementPresent(WebElement ele){
		return ele.isDisplayed();
	}
	
	protected String getElementText(WebElement ele) {
		return ele.getText();
	}
	
	protected void dropDownSelectByIndex(WebElement ele, int index) {
		
		Select fTxtBox =new Select(ele);
		
		fTxtBox.selectByIndex(index);
		
	}
	protected void dropDownSelectByText(WebElement ele,String text) {
		
		Select select =new Select(ele);
		select.selectByVisibleText(text);
		
	}
	protected List<WebElement> dropDownGetOptions(WebElement ele) {
		Select select =new Select(ele);
		return select.getOptions();
		
	}
	protected void mouseOver(WebElement ele) {
		Actions action =new Actions(driver);
		action.moveToElement(ele).perform();
	}
	
	protected void clickElement(WebElement ele) {
		
		ele.click();
	}
}
